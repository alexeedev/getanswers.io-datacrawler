package io.getanswers.crawler.stackoverflow.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class BaseSOResponse {

	@JsonProperty("has_more")
	public boolean hasMore;
	
	@JsonProperty("quota_max")
	public int quotaMax;
	
	@JsonProperty("quota_remaining")
	public int quotaRemaining;
	
}
