package io.getanswers.crawler.stackoverflow.api;

import java.util.List;


public interface IRequestCallback {

	public void onResponse(boolean success, IRawResponse questionsResponse, List<IRawResponse> answersResponse);
}
