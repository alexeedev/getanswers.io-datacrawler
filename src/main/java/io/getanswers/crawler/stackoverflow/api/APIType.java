package io.getanswers.crawler.stackoverflow.api;

public class APIType {

	public static final String QUESTIONS_PER_RANGE = "questions_per_range";
	public static final String QUESTIONS_PER_RANGE_ANSWERS = "questions_per_range_answers";
}
