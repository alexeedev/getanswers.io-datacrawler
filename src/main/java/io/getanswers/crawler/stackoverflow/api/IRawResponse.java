package io.getanswers.crawler.stackoverflow.api;

public interface IRawResponse {

	public String getInputAsString();
	
	public String getResponseJson();

	public String getDatatype();
	
}
