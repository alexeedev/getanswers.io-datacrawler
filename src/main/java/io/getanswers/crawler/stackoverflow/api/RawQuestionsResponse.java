package io.getanswers.crawler.stackoverflow.api;

import java.util.List;


public class RawQuestionsResponse implements IRawResponse {

	private String idsRange;
	private String response;
	private List<Integer> ids;
	
	public RawQuestionsResponse(List<Integer> ids, String idsRange, String response) {
		this.idsRange = idsRange;
		this.response = response;
		this.ids = ids;
	}

	@Override public String getInputAsString() {
		return idsRange;
	}

	@Override public String getResponseJson() {
		return response;
	}
	
	public List<Integer> getIds() {
		return ids;
	}

	@Override public String getDatatype() {
		return APIType.QUESTIONS_PER_RANGE;
	}
	
}
