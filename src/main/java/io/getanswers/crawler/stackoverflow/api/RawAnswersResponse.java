package io.getanswers.crawler.stackoverflow.api;

import java.util.List;

public class RawAnswersResponse implements IRawResponse {

	private String idsRange;
	private String response;
	private List<Integer> ids;
	private boolean hasMore;
	
	public RawAnswersResponse(List<Integer> ids, String idsRange, String response, boolean hasMore) {
		this.idsRange = idsRange;
		this.response = response;
		this.ids = ids;
		this.hasMore = hasMore;
	}

	
	public boolean isHasMore() {
		return hasMore;
	}


	@Override public String getInputAsString() {
		return idsRange;
	}

	@Override public String getResponseJson() {
		return response;
	}
	
	public List<Integer> getIds() {
		return ids;
	}

	@Override public String getDatatype() {
		return APIType.QUESTIONS_PER_RANGE_ANSWERS;
	}
	
}