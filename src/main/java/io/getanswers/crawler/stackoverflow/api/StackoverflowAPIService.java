package io.getanswers.crawler.stackoverflow.api;

import io.getanswers.crawler.service.StackoverflowQuotaService;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;



@Service
public class StackoverflowAPIService {
	
	@Value("${so_key}")
	private String so_key;
	
	@Autowired
	private StackoverflowQuotaService quotaService;

	public void requestQuestionsForRange(List<Integer> ids, IRequestCallback requestCallback) {
		try {
			
			RawQuestionsResponse questionsResponse = requestQuestions(ids);
			List<IRawResponse> answers = new ArrayList<IRawResponse>();
			int page = 1;
			while (true) {
				RawAnswersResponse answersResponse = requestAnswers(ids, page);
				answers.add(answersResponse);
				if (answersResponse.isHasMore()) {
					page++;
				} else {
					break;
				}
			}
		    requestCallback.onResponse(true, questionsResponse, answers);
		    
		} catch (Exception e) {
			e.printStackTrace();
			requestCallback.onResponse(false, null, null);
		}
	}
	
	private RawAnswersResponse requestAnswers(List<Integer> ids, int pageId) throws Exception {
		String range = "";
		for (int i = 0; i < ids.size(); i++) {
			range = range + "" + ids.get(i);
			if (i < ids.size() - 1) {
				range += ";";
			}
		}
		String urlString = "https://api.stackexchange.com/2.2/questions/" + range + "/answers?key=" + so_key + "&page=" + pageId + "&order=desc&sort=activity&site=stackoverflow&filter=!-*f(6t*ZdJQL";
        URLConnection connection = new URL(urlString).openConnection();                        
        BufferedReader in = null;
        connection.setReadTimeout(10000);
	    in = new BufferedReader(new InputStreamReader(new GZIPInputStream(connection.getInputStream())));            
	    String json = in.readLine();
	    ObjectMapper objectMapper = new ObjectMapper();
	    BaseSOResponse soResponse = objectMapper.readValue(json, BaseSOResponse.class);
	    RawAnswersResponse response = new RawAnswersResponse(ids, range, json, soResponse.hasMore);
	    quotaService.setQuota(soResponse.quotaRemaining, soResponse.quotaMax);
	    in.close();
	    return response;
	}

	private RawQuestionsResponse requestQuestions(List<Integer> ids) throws Exception {
		String range = "";
		for (int i = 0; i < ids.size(); i++) {
			range = range + "" + ids.get(i);
			if (i < ids.size() - 1) {
				range += ";";
			}
		}
		String urlString = "https://api.stackexchange.com/2.2/questions/" + range + "?key=" + so_key + "&order=desc&sort=activity&site=stackoverflow&filter=!9YdnSIoKx";
        URLConnection connection = new URL(urlString).openConnection();                        
        BufferedReader in = null;
        connection.setReadTimeout(10000);
	    in = new BufferedReader(new InputStreamReader(new GZIPInputStream(connection.getInputStream())));            
	    String json = in.readLine();
	    RawQuestionsResponse response = new RawQuestionsResponse(ids, range, json);
	    ObjectMapper objectMapper = new ObjectMapper();
	    BaseSOResponse soResponse = objectMapper.readValue(json, BaseSOResponse.class);
	    quotaService.setQuota(soResponse.quotaRemaining, soResponse.quotaMax);
	    in.close();
	    return response;
	}

	public static String decompress(String str) throws Exception {
        if (str == null || str.length() == 0) {
            return str;
        }
        GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(str.getBytes()));
        BufferedReader bf = new BufferedReader(new InputStreamReader(gis, "UTF-8"));
        String outStr = "";
        String line;
        while ((line=bf.readLine())!=null) {
          outStr += line;
        }
        return outStr;
     }


}
