package io.getanswers.crawler.service;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class StackoverflowQuotaService {

	private static final Logger logger = Logger.getLogger(StackoverflowQuotaService.class);
	
	private int quotaRemaining;
	private boolean quotaSet;
	
	public boolean hasAvailableQuota() {
		if (quotaSet) {
			return quotaRemaining > 50;
		} else {
			return true;
		}
	}
	
	public void setQuota(int quotaRemaining, int quotaMax) {
		this.quotaRemaining = quotaRemaining;
		logger.info("Quota: " + quotaRemaining + "/" + quotaMax);
	}
	
}
