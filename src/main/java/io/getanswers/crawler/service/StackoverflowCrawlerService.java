package io.getanswers.crawler.service;

import io.getanswers.crawler.stackoverflow.api.IRawResponse;
import io.getanswers.crawler.stackoverflow.api.IRequestCallback;
import io.getanswers.crawler.stackoverflow.api.RawQuestionsResponse;
import io.getanswers.crawler.stackoverflow.api.StackoverflowAPIService;
import io.getanswers.domain.model.RawData;
import io.getanswers.domain.model.StackoverflowQuestionsMeta;
import io.getanswers.domain.repository.RawDataRepository;
import io.getanswers.domain.repository.StackoverflowQuestionsMetaRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class StackoverflowCrawlerService {

	private static final int MAX_IDS_IN_REQUEST = 100;
	private static final Logger logger = Logger.getLogger(StackoverflowCrawlerService.class);
	
	@Autowired
	private StackoverflowAPIService soApi;
	
	@Autowired
	private StackoverflowQuotaService soQuotaService;
	
	@Autowired
	private RawDataRepository rawDataRepository;
	
	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private StackoverflowQuestionsMetaRepository soQuestionsMetaRepository;
	
	@Value("${so.scan.questions.range.to}")
	private int maxQuestionsPerRangeId;
	
	@Value("${so.scan.questions.range.from}")
	private int minQuestionsPerRangeId;
	
	public void continueCrawlingByRange() {
		while (true) {
			if (!soQuotaService.hasAvailableQuota()) {
				logger.error("Stackoverflow API quota limit is reached, terminating..");
				return;
			}
			List<Integer> ids = getIdsForCrawling();
			if (ids.isEmpty()) {
				logger.error("All done, terminating..");
				return;
			}
			logger.info("Crawling ids from .. " +ids.get(0));
			soApi.requestQuestionsForRange(ids, new IRequestCallback() {
				@Override public void onResponse(boolean success, IRawResponse questionsResponse, List<IRawResponse> answersResponse) {
					saveQuestionsByRangeResponse(questionsResponse);
					for (IRawResponse answerResponse : answersResponse) {
						saveAnswersByRangeResponse(answerResponse);
					}
					RawQuestionsResponse rawQuestionsResponse= (RawQuestionsResponse) questionsResponse;
					for (Integer id : rawQuestionsResponse.getIds()) {
						StackoverflowQuestionsMeta meta = new StackoverflowQuestionsMeta();
						meta.parsedDate = new Date();
						meta.questionId = id;
						soQuestionsMetaRepository.save(meta);
					}
				}
			});
		}
	}

	private int getLastCompletedId() {
		Integer id = soQuestionsMetaRepository.getLastId();
		return id == null ? 0 : id;
	}

	private List<Integer> getIdsForCrawling() {
		int lastId = getLastCompletedId();
		List<Integer> ret = new ArrayList<Integer>();
		int minId = lastId + 1;
		minId = Math.max(minId, minQuestionsPerRangeId);
		int maxId = minId + MAX_IDS_IN_REQUEST - 1;
		maxId = Math.min(maxId, maxQuestionsPerRangeId);
		for (int i = minId; i <= maxId; i++) {
			ret.add(i);
		}
		return ret;
	}

	protected void saveQuestionsByRangeResponse(IRawResponse response) {
		RawData rawData = new RawData();
		rawData.datatype = response.getDatatype();
		rawData.input = response.getInputAsString();
		rawData.output = response.getResponseJson();
		rawDataRepository.save(rawData);
	}
		
	protected void saveAnswersByRangeResponse(IRawResponse response) {
		try {
			RawData rawData = new RawData();
			rawData.datatype = response.getDatatype();
			rawData.input = response.getInputAsString();
			rawData.output = response.getResponseJson();
			rawDataRepository.save(rawData);
		} catch (Exception e) {
			// noop
		}
	}

	
}
