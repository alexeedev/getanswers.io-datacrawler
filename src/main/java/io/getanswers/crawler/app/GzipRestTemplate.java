package io.getanswers.crawler.app;

import java.io.IOException;
import java.net.URI;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.web.client.RestTemplate;

public class GzipRestTemplate extends RestTemplate {
    @Override
    protected ClientHttpRequest createRequest(URI url, HttpMethod method)
            throws IOException {
        ClientHttpRequest request = super.createRequest(url, method);
        HttpHeaders headers = request.getHeaders();
        headers.set("Accept-Encoding", "gzip");
        return request;
    }
}
