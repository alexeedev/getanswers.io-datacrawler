package io.getanswers.crawler.app;

import io.getanswers.crawler.service.StackoverflowCrawlerService;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("io.getanswers")
@EnableAutoConfiguration
@EnableJpaRepositories("io.getanswers.domain.repository")
@EntityScan("io.getanswers.domain.model")
public class DataCrawlerApplication {

	@Autowired
	private StackoverflowCrawlerService crawlerService;
	
	public static void main(String[] args) {
		 ApplicationContext ctx = SpringApplication.run(DataCrawlerApplication.class, args);
		 System.exit(SpringApplication.exit(ctx));
	}

	@PostConstruct
	public void start() {
		crawlerService.continueCrawlingByRange();
	}
	
}
